<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 10:36
 */

use Life\Dimension;

class DimensionTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @param $width
     * @param $height
     * @dataProvider invalidDimensions
     * @expectedException \InvalidArgumentException
     */
    public function testInvalidDimensions($width, $height)
    {
        new Dimension($width, $height);
    }

    public function invalidDimensions()
    {
        return [
            [0,0],
            [10,-10],
            [0.1,0.1],
            [-0.1,0.1],
            [-0.1,-0.1],
            [-1,-1],
        ];
    }
}
