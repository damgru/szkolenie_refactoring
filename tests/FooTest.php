<?php

namespace Test\Life;

use Life\Dice;
use PHPUnit\Framework\TestCase;

class FooTest extends TestCase
{
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testExceptions()
    {
        //$this->expectException(\InvalidArgumentException::class);

        new Dice(-8);
    }

    public function testDiceRoll()
    {
        // Arrange
        $dice = $this->prophesize(Dice::class);

        $dice->roll()->willReturn(3);

        $dice = $dice->reveal();

        // Act
        $result = $dice->roll();

        // Assert
        $this->assertEquals(3, $result);
    }

    public function testDiceRollCall()
    {
        // Arrange
        $dice = $this->prophesize(Dice::class);

        $dice->roll()->shouldBeCalled();

        $dice = $dice->reveal();

        // Act
        $dice->roll();
    }

    /**
     * @dataProvider getData
     */
    public function testFoo1($a, $b, $expectedResult)
    {
        // Arrange

        // Act
        $c = $a + $b;

        // Assert
        $this->assertEquals($expectedResult, $c);
    }

    public function getData()
    {
        return [
            "negative first" => [-1, 0, -1],
            "both positive" => [1, 2, 3],
            "positive & zero" => [0, 1, 1],
        ];
    }
}