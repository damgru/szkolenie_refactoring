<?php

namespace Test\Life;

use Life\Cell;
use Life\Game;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    public function testBlinker()
    {
        $game = $this->prepareGame('test_before');
        $gameToCompare = $this->prepareGame('test_after');

        ob_start();
        $game->iterate();

        $this->assertEquals(
            $this->cellsToString($gameToCompare->grid->getCells()),
            $this->cellsToString($game->grid->getCells())
        );

        ob_get_clean();
    }

    private function cellsToString($cells)
    {
        $result = '';
        foreach ($cells as $row)
        {
            foreach ($row as $cell)
            {
                /** @var Cell $cell */
                $result .= $cell->isAlive() ? '1' : '0';
            }
            $result .= "\n";
        }
        return $result;
    }

    private function prepareGame(string $template)
    {
        return new Game([
            'realtime' => FALSE,
            'max_frame_count' => 1,
            'template' => $template,
            'random' => FALSE,
            'width' => 10,
            'height' => 3,
            'cell' => 'O',
            'empty' => ' ',
            'renderer' => 'file'
        ]);
    }
}