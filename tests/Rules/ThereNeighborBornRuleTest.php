<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 10:06
 */

namespace Rules;
use Life\ChangeCell\ResurrectChangeCell;
use Life\Position;
use Life\Rules\ThereNeighborBornRule;

class ThereNeighborBornRuleTest extends RuleTestCase
{
    public function testThatCellWillBornWithThereNeighbor()
    {
        //Arrange
        $rule = new ThereNeighborBornRule();
        $grid = $this->prepareMockedGrid(3,0);
        $position = new Position(2,1);

        //ACT
        /** @var \Life\ChangeCell[] $changes */
        $changes = $rule->getChanges($grid, $position);

        //Assert
        $this->assertCount(1, $changes);
        $this->assertEquals($changes[0], new ResurrectChangeCell($position));
    }

    /**
     * @dataProvider doesNothingProvider
     */
    public function testThatDoesNothing($aliveNeighbors, $cellStatus)
    {
        //Arrange
        //Arrange
        $rule = new ThereNeighborBornRule();
        $grid = $this->prepareMockedGrid(1,0);
        $position = new Position(2,1);

        //ACT
        /** @var \Life\ChangeCell[] $changes */
        $changes = $rule->getChanges($grid, $position);
        //Assert
        $this->assertCount(0, $changes, var_export($changes, true));
    }

    public function doesNothingProvider()
    {
        return [
            [0, 0],
            [1, 0],
            [2, 0],
            [4, 0],
            [5, 0],
            [6, 0],
            [7, 0],
            "not_ressurecting_when_alive" => [3, 1],
        ];
    }
}
