<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 08:30
 */

namespace Rules;
use Life\ChangeCell\KillChangeCell;
use Life\Position;
use Life\Rules\LonelyDieRule;


class LonelyDieRuleTest extends RuleTestCase
{
    public function testThatLonelyCellDies()
    {
        //Arrange
        $rule = new LonelyDieRule();
        $grid = $this->prepareMockedGrid(0,1);
        $position = new Position(2, 1);

        //ACT
        /** @var \Life\ChangeCell[] $changes */
        $changes = $rule->getChanges($grid, $position);

        //Assert
        $this->assertCount(1, $changes);
        $this->assertEquals($changes[0], new KillChangeCell($position));
    }
}
