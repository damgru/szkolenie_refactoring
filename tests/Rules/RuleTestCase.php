<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 10:09
 */

namespace Rules;
use Life\Cell;
use Life\Grid;
use Life\Position;
use PHPUnit\Framework\TestCase;

abstract class RuleTestCase extends TestCase
{
    /** @return Grid */
    protected function prepareMockedGrid($aliveNeighbors = 0, $cellStatus = 1)
    {
        $grid = $this->prophesize(\Life\Grid::class);
        $grid->countAliveNeighbors(\Prophecy\Argument::type("int"), \Prophecy\Argument::type('int'))->willReturn($aliveNeighbors);
        $cell = new Cell($cellStatus);

        $grid->getCell(\Prophecy\Argument::any())->willReturn($cell);

        return $grid->reveal();
    }
}