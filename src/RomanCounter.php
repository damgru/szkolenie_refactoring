<?php

namespace Life;

use Romans\Filter\IntToRoman;

class RomanCounter implements PatternCounter
{
    /**
     * @var PatternCounter
     */
    private $counter;

    /**
     * RomanCounter constructor.
     * @param PatternCounter $counter
     */
    public function __construct(PatternCounter $counter)
    {
        $this->counter = $counter;
    }

    public function count(Grid $grid): string
    {
        return (new IntToRoman())->filter($this->counter->count($grid));
    }

    public function getName(): string
    {
        return $this->counter->getName();
    }
}