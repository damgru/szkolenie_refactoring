<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 13:32
 */

namespace Life;

use Life\ChangeCell\AddLivesChangeCell;
use Life\ChangeCell\KillChangeCell;
use Life\ChangeCell\MakeUnkillableChangeCell;
use Life\ChangeCell\ResurrectChangeCell;
use Webmozart\Assert\Assert;

class Cell
{
    private $godMode = false;
    private $lives = 1;

    public function __construct($state = 1)
    {
        $this->godMode = false;

        if(empty($state))
        {
            $this->lives = 0;
        }
        else {
            $this->lives = 1;
        }
    }

    public function isKilled()
    {
        return ($this->lives <= 0);
    }

    public function isAlive()
    {
        return $this->lives > 0;
    }

    public function isUnkillable()
    {
        return $this->godMode;
    }

    public function kill($times = 1)
    {
        Assert::greaterThan($times, 0);
        if($this->isKilled()) {
            return;
        }

        if($this->godMode) {
            return;
        }

        $this->lives-=$times;
    }

    public function makeUnkillable()
    {
        $this->godMode = true;
    }

    public function addLives($numberOfLives)
    {
        $this->lives += $numberOfLives;
    }

    public function getStatus()
    {
        return $this->isAlive() ? $this->lives : 0;
    }

    public function apply(ChangeCell $change)
    {
        //$this->state->apply($change);

        switch (get_class($change))
        {
            case KillChangeCell::class:
                /** @var KillChangeCell $change */
                $this->kill($change->getTimes());
                break;
            case ResurrectChangeCell::class:
                $this->lives = 1;
                break;
            case MakeUnkillableChangeCell::class:
                /** @var MakeUnkillableChangeCell $change */
                $this->makeUnkillable();
                break;
            case AddLivesChangeCell::class:
                /** @var AddLivesChangeCell $change */
                $this->addLives($change->getLives());
                break;
            default:
                throw new \RuntimeException('ChangeCell not implemented');
        }
    }

}