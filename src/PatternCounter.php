<?php

namespace Life;

interface PatternCounter
{
    public function count(Grid $grid): string;

    public function getName(): string;
}