<?php

namespace Life;

interface GameCellFrameRule
{
    public function getChanges(Grid $grid, Position $position) : array ;
}