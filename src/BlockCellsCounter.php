<?php

namespace Life;

class BlockCellsCounter implements PatternCounter
{
    public function count(Grid $grid): string
    {
        return 42;
    }

    public function getName(): string
    {
        return 'Block';
    }
}