<?php
/**
 * @file
 * Contains the game controller.
 */

namespace Life;

use Life\ChangeCell\AddLivesChangeCell;
use Life\ChangeCell\KillChangeCell;
use Life\ChangeCell\MakeUnkillableChangeCell;
use Life\ChangeCell\ResurrectChangeCell;
use Life\Renderer\CliRenderer;
use Life\Renderer\FileRenderer;
use Life\Renderer\RendererFactory;
use Life\Rules\LonelyDieRule;
use Life\Rules\ThereNeighborBornRule;

/**
 * Class Game
 *
 * Controller used for instantiating a new game.
 *
 * @package Life
 */
class Game
{

    private $opts = [];
    /**
     * @var PatternCounter[]
     */
    private $counters = [];
    /** @var CliRenderer */
    private $renderer;
    private $start_time = 0;
    private $frame_count = 0;

    // @todo setDefaults - czy to jest ok?
    private function setDefaults(array $opts)
    {
        $defaults = [
            'timeout' => 5000,
            'rand_max' => 5,
            'realtime' => TRUE,
            'max_frame_count' => 0,
            'template' => NULL,
            'random' => TRUE,
            'width' => exec('tput cols'),
            'height' => exec('tput lines') - 3,
            'cell' => '0',
            'empty' => ' ',
            'renderer' => 'cli'
        ];
        if (isset($opts['template']) && !isset($opts['random'])) {
            // Disable random when template is set.
            $opts['random'] = FALSE;
        }
        $opts += $defaults;
        $this->opts += $opts;
    }

    public function __construct(array $opts)
    {
        $this->setDefaults($opts);
        $this->start_time = time();
        $this->grid = new Grid(new Dimension($this->opts['width'], $this->opts['height']));
        $this->grid->generateCells($this->opts['random'], $this->opts['rand_max']);

        if (!empty($this->opts['template'])) {
            $this->loadTemplateByName($this->opts['template']);
        }

        $this->counters = [
            new DeadCellsCounter(),
            new BlockCellsCounter(),
            new RomanCounter(new LiveCellsCounter()),
        ];

        $this->renderer = RendererFactory::createRenderer($this->opts['renderer'], $this->opts, $this->counters);

        $this->rules = [
            new LonelyDieRule(),
            new ThereNeighborBornRule()
        ];
    }

    public function iterate()
    {
        while (TRUE) {
            $this->frame_count++;

            if ($this->opts['realtime']) {
                $this->render();

                usleep($this->opts['timeout']);
            }

            $this->applyGameRules();

            if ($this->isMaxFrameReached($this->getMaxFrameCount(), $this->frame_count)) {
                break;
            }
        }

        if (!$this->opts['realtime']) {
            $this->render(false);
        }
    }

    /**
     * @return mixed
     */
    protected function getMaxFrameCount()
    {
        return $this->opts['max_frame_count'];
    }

    /**
     * @return bool
     */
    protected function isMaxFrameReached( $maxFrameCount, $frameCount): bool
    {
        return $maxFrameCount && $frameCount >= $maxFrameCount;
    }

    public function render(bool $withFooter = true)
    {
        $this->renderer->render($this->grid, $withFooter, $this->frame_count, $this->start_time);
    }


    // @todo
    public function loadTemplateByName($name)
    {
        $path = 'templates/' . $name . '.txt';
        $file = fopen($path, 'r');
        $centerX = (int)floor($this->grid->getWidth() / 2) / 2;
        $centerY = (int)floor($this->grid->getHeight() / 2) / 2;
        $x = $centerX;
        $y = $centerY;
        while ($c = fgetc($file)) {
            if ($c == 'O') {
                $this->grid->ressurectCell(new Position($x, $y));
            }
            if ($c == "\n") {
                $y++;
                $x = $centerX;
            } else {
                $x++;
            }
        }
        fclose($file);
    }

    /**
     * Processes a new generation for all cells.
     *
     * Base on these rules:
     * 1. Any live cell with fewer than two live neighbours dies, as if by needs caused by underpopulation.
     * 2. Any live cell with more than three live neighbours dies, as if by overcrowding.
     * 3. Any live cell with two or three live neighbours lives, unchanged, to the next generation.
     * 4. Any dead cell with exactly three live neighbours will come to life.
     */
    private function applyGameRules()
    {
        $changes = [];
            for ($x = 0; $x < $this->grid->getWidth(); $x++) {
                for ($y = 0; $y < $this->grid->getHeight(); $y++) {
                /** @var GameCellFrameRule $rule */
                foreach ($this->rules as $rule)
                {
                    $changes = array_merge($changes, $rule->getChanges($this->grid, new Position($x, $y)));
                }
            }
        }

        /** @var ChangeCell $change */
        foreach ($changes as $change) {
            $this->applyChange($change);
        }
    }

    private function applyChange($change)
    {
        /** @var ChangeCell $change */

        $cell =  $this->grid->getCell($change->getPosition());
        $cell->apply($change);
    }
}