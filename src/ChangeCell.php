<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 11.12.2018
 * Time: 15:01
 */

namespace Life;

use Life\Grid;

abstract class ChangeCell
{
    private $p;

    /**
     * ChangeCell constructor.
     * @param $x
     * @param $y
     */
    public function __construct(Position $p)
    {
        $this->position = $p;
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->position->getX();
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->position->getY();
    }

    public function getPosition() : Position
    {
        return $this->position;
    }


}