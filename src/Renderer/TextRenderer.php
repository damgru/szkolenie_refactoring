<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 10:58
 */

namespace Life\Renderer;

use Life\Grid;
use Life\PatternCounter;

class TextRenderer implements Renderer
{
    private $opts = [
        'cell' => 'O',
        'empty' => ' '
    ];

    /** @var PatternCounter[] */
    private $counters = [];

    private $textRendered;
    private $output;

    /**
     * CliRenderer constructor.
     * @param array $opts
     * @param $grid
     */
    public function __construct(array $opts, $counters = [], TextOutputInterface $output)
    {
        $this->output = $output;
        $this->opts = array_merge($this->opts, $opts);
        $this->counters = $counters;
    }

    public function render(Grid $grid, bool $withFooter = true, $frameCount = 0, $startCount = 0)
    {
        $this->renderCells($grid);

        if ($withFooter) {
            $this->renderFooter($grid, $frameCount, $startCount);
        }

        $this->output->write($this->textRendered);
        $this->textRendered = '';
    }

    /**
     * Renders the grid in the terminal window.
     */
    private function renderCells(Grid $grid)
    {
        foreach ($grid->getCells() as $y => $row) {
            foreach ($row as $x => $cell) {
                /** @var Cell $cell */
                $this->textRendered .= ($cell ? $this->opts['cell'] : $this->opts['empty']);
            }
            // Done with the row.
            $this->textRendered .= "\n";
        }
    }

    /**
     * Renders a footer below the playing game.
     */
    private function renderFooter(Grid $grid, $frameCount, $startCount)
    {
        $this->textRendered .= str_repeat('_', $this->opts['width']) . "\n";
        $this->textRendered .=  $this->getStatus($grid, $frameCount, $startCount) . "\n";
    }

    /**
     * Gets a status string with various attributes.
     *
     * @return string
     */
    private function getStatus($grid, $frameCount, $startCount)
    {
        $elapsed_time = time() - $startCount;
        $fps = $elapsed_time > 0
            ? number_format($frameCount / $elapsed_time, 1)
            : 'N/A';

        return sprintf(
            "Gen: %d | %s | Elapsed Time: %d | FPS: %s",
            $frameCount,
            $this->renderCounters($grid),
            $elapsed_time,
            $fps
        );
    }

    private function renderCounters(Grid $grid): string
    {
        $result = '';

        foreach ($this->counters as $counter) {
            $result .= $counter->getName() . ': ' . $counter->count($grid) . ' | ';
        }

        return $result;
    }

}