<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 11:41
 */

namespace Life\Renderer;


interface TextOutputInterface
{
    public function write($frameText);
}