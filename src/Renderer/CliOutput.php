<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 11:48
 */

namespace Life\Renderer;

class CliOutput implements TextOutputInterface
{

    public function write($frameText)
    {
        $this->resetCursorPosition();
        echo $frameText;
    }

    /**
     * Moves the cursor back to (0,0) to overwrite the screen.
     */
    protected function resetCursorPosition()
    {
        // Move to (0,0).
        echo "\033[0;0H";
    }
}
