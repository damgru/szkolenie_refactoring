<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 11:11
 */

namespace Life\Renderer;

use Life\Grid;

interface Renderer
{
    public function render(Grid $grid, bool $withFooter = true, $frameCount = 0, $startCount = 0);
}