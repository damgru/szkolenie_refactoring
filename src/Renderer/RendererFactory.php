<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 11:30
 */

namespace Life\Renderer;


class RendererFactory
{
    public static function createRenderer(string $type, $opts, $counters)
    {
        switch ($type) {
            case 'file':
                return new TextRenderer($opts, $counters, new FileOutput());
            case 'cli':
            default:
                return new TextRenderer($opts, $counters, new CliOutput());
        }
    }

}