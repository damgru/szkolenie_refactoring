<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 11:52
 */

namespace Life\Renderer;


class FileOutput implements TextOutputInterface
{
    private $filePath;

    /**
     * FileOutput constructor.
     * @param $filePath
     */
    public function __construct($filePath = '')
    {
        $this->filePath = empty($filePath)
            ? __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'text.txt'
            : __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $filePath;

        file_put_contents($this->filePath, '');
    }


    public function write($frameText)
    {
        file_put_contents($this->filePath, $frameText."\n\n", FILE_APPEND);
    }
}