<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 14:06
 */

namespace Life\ChangeCell;


use Life\ChangeCell;
use Life\Position;

class AddLivesChangeCell extends ChangeCell
{
    private $lives = 1;

    /**
     * AddLivesChangeCell constructor.
     * @param int $lives
     */
    public function __construct(Position $position, int $lives = 1)
    {
        $this->lives = $lives;
        parent::__construct($position);
    }

    /**
     * @return int
     */
    public function getLives(): int
    {
        return $this->lives;
    }

}