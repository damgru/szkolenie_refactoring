<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 13:49
 */

namespace Life\ChangeCell;

use Life\ChangeCell;
use Life\Position;

class KillChangeCell extends ChangeCell
{
    private $times;

    /**
     * KillChangeCell constructor.
     * @param $times
     */
    public function __construct(Position $position, int $times = 1)
    {
        $this->times = $times;
        parent::__construct($position);
    }

    /**
     * @return int
     */
    public function getTimes(): int
    {
        return $this->times;
    }

}