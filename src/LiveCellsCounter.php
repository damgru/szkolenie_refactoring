<?php

namespace Life;

class LiveCellsCounter implements PatternCounter
{
    public function count(Grid $grid): string
    {
        $count = 0;

        foreach ($grid->getCells() as $y => $row) {
            foreach ($row as $x => $cell) {
                if ($cell) {
                    $count++;
                }
            }
        }

        return $count;
    }

    public function getName(): string
    {
        return 'Live';
    }
}