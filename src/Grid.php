<?php

namespace Life;

use Couchbase\Exception;
use Webmozart\Assert\Assert;

class Grid
{
    private $cells = [];
    /** @var Dimension  */
    private $dimension;

    public function __construct(Dimension $dimension)
    {
        $this->dimension = $dimension;
    }

    public function generateCells($randomize, $rand_max = 10)
    {
        for ($x = 0; $x < $this->getWidth(); $x++) {
            for ($y = 0; $y < $this->getHeight(); $y++) {
                if ($randomize) {
                    $this->cells[$y][$x] = new Cell($this->getRandomState($rand_max));
                } else {
                    $this->cells[$y][$x] = new Cell(0);
                }
            }
        }
        return $this;
    }

    public function getWidth()
    {
        return $this->dimension->getWidth();
    }

    public function getHeight()
    {
        return $this->dimension->getHeight();
    }

    private function getRandomState($rand_max = 1)
    {
        return rand(0, $rand_max) === 0;
    }

    public function ressurectCell(Position $p)
    {
        $this->cells[$p->getY()][$p->getX()] = new Cell();
    }

    /**
     * @return array
     */
    public function getCells(): array
    {
        return $this->cells;
    }

    public function getCell(Position $p) : Cell
    {
        return $this->cells[$p->getY()][$p->getX()];
    }

    /**
     * Gets living neighbors for a cell at given coordinates.
     *
     * @param int $x
     * @param int $y
     *
     * @return int
     *   Returns the number of alive neighbors for this cell.
     */
    public function countAliveNeighbors($x, $y)
    {
        $alive_count = 0;
        for ($y2 = $y - 1; $y2 <= $y + 1; $y2++) {
            if ($y2 < 0 || $y2 >= $this->getHeight()) {
                // Out of range.
                continue;
            }
            for ($x2 = $x - 1; $x2 <= $x + 1; $x2++) {
                if ($x2 == $x && $y2 == $y) {
                    // Current cell spot.
                    continue;
                }
                if ($x2 < 0 || $x2 >= $this->getWidth()) {
                    // Out of range.
                    continue;
                }
                if ($this->getCell(new Position($x2, $y2))->isAlive()) {
                    $alive_count += 1;
                }
            }
        }
        return $alive_count;
    }
}
