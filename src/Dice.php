<?php

namespace Life;

class Dice
{
    private $sides;

    /**
     * Dice constructor.
     * @param $sides
     */
    public function __construct(int $sides)
    {
        if ($sides < 2) {
            throw new \InvalidArgumentException();
        }

        $this->sides = $sides;
    }

    public function roll(): int
    {
        return random_int(1, $this->sides);
    }
}