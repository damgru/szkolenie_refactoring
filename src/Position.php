<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 13:44
 */

namespace Life;


use Webmozart\Assert\Assert;

class Position
{
    protected $x;
    protected $y;

    /**
     * Position constructor.
     * @param $x
     * @param $y
     */
    public function __construct($x, $y)
    {
        Assert::greaterThanEq($x, 0);
        Assert::greaterThanEq($y, 0);

        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }



}