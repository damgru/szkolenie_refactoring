<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 11.12.2018
 * Time: 15:45
 */

namespace Life\Rules;

use Life\ChangeCell;
use Life\GameCellFrameRule;
use Life\Grid;
use Life\Position;

class ThereNeighborBornRule implements GameCellFrameRule
{
    public function getChanges(Grid $grid, Position $position): array
    {
        $neighbor_count = $grid->countAliveNeighbors($position->getX(), $position->getY());
        $cell = $grid->getCell($position);

        if ($cell->isKilled() && $neighbor_count === 3) {
            return [new ChangeCell\ResurrectChangeCell($position)];
        }

        return [];
    }
}