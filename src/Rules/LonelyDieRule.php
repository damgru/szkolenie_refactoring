<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 11.12.2018
 * Time: 15:00
 */

namespace Life\Rules;

use Life\ChangeCell;
use Life\ChangeCell\KillChangeCell;
use Life\GameCellFrameRule;
use Life\Grid;
use Life\Position;

class LonelyDieRule implements GameCellFrameRule
{
    public function getChanges(Grid $grid, Position $position): array
    {
        $neighbor_count = $grid->countAliveNeighbors($position->getX(), $position->getY());
        $cell = $grid->getCell($position);

        if ($cell->isAlive() && ($neighbor_count < 2 || $neighbor_count > 3)) {
            return [new KillChangeCell($position)];
        }

        return [];
    }
}