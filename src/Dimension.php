<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.12.2018
 * Time: 10:29
 */

namespace Life;


use Webmozart\Assert\Assert;

final class Dimension
{
    /** @var int */
    private $width;
    /** @var int */
    private $height;

    /**
     * Dimension constructor.
     * @param int $width
     * @param int $height
     */
    public function __construct(int $width, int $height)
    {
        Assert::greaterThan($width, 0);
        Assert::greaterThan($height, 0);

        $this->width = $width;
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }
}